#include "tst_test_vvid.h"
#include <../functions.h>
#include <QtTest>

test_vvid::test_vvid()
{

}

void test_vvid::test_issue1()
{
    int test_n_before = 111;
    bool a = function1(test_n_before);
    QCOMPARE(7, test_n_before);

    test_n_before = 221;
    a = function1(test_n_before);
    QCOMPARE(false, a);
}
void test_vvid::test_issue2()
{
    int test_n_before = 190;
    bool a = function2(test_n_before);
    QCOMPARE(10111110, test_n_before);
}
void test_vvid::test_issue3()
{

    int test_n_before = 111;
    bool a = function3(test_n_before);
    QCOMPARE(7, test_n_before);

    test_n_before = 221;
    a = function1(test_n_before);
    QCOMPARE(false, a);

}
void test_vvid::test_issue4()
{
    int test_n_before = 17;
    bool a = function4(test_n_before);
    QCOMPARE(1111, test_n_before);

    test_n_before = 99;
    a = function4(test_n_before);
    QCOMPARE(false, a);

}
void test_vvid::test_issue5()
{
    int test_n_before = 11;
    bool a = function5(test_n_before);
    QCOMPARE(9, test_n_before);

    test_n_before = 10;
    a = function5(test_n_before);
    QCOMPARE(8, test_n_before);
}
void test_vvid::test_issue6()
{

    int test_n_before = 22;
    bool a = function6(test_n_before);
    QCOMPARE(26, test_n_before);

    test_n_before = 15;
    a = function6(test_n_before);
    QCOMPARE(17, test_n_before);
}

