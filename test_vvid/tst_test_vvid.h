#ifndef TST_TEST_VVID_H
#define TST_TEST_VVID_H
#include <QtTest>

class test_vvid : public QObject
{
    Q_OBJECT

public:
    test_vvid();

private slots:
    void test_issue1();
    void test_issue2();
    void test_issue3();
    void test_issue4();
    void test_issue5();
    void test_issue6();


};
#endif // TST_TEST_VVID_H
