#include <QDebug>
#include <iostream>
#include "functions.h"
#include <cmath>


using namespace std;

int main()
{
    int menu = 0;
    int n = 0;
    do
    {
        qDebug() << "1. Convert 2 to 10" << endl;
        qDebug() << "2. Convert 10 to 2" << endl;
        qDebug() << "3. Convert 2 to 8" << endl;
        qDebug() << "4. Convert 8 to 2" << endl;
        qDebug() << "5. Convert 8 to 16" << endl;
        qDebug() << "6. Convert 16 to 8" << endl;
        qDebug() << "0. Exit" << endl;
        qDebug() << "Select a menu item" << endl;
        char proverka = '\0';
        while (scanf("%d%c", &menu, &proverka, 1) != 2 || proverka != '\n' || (menu > 6 && menu < 0))
        {
            qDebug() << "Error!" << endl;
            while (getchar() != '\n');
        }
        switch(menu)
        {
            case 1:
            {
                qDebug() << "Enter the number" << endl;
                while (scanf("%d%c", &n, &proverka, 1) != 2 || proverka != '\n')
                {
                    qDebug() << "Error!" << endl;
                    while (getchar() != '\n');
                }
                bool a = function1(n);
                if (a)
                    qDebug() << n << endl;
                break;
            }
            case 2:
            {
                qDebug() << "Enter the number" << endl;
                while (scanf("%d%c", &n, &proverka, 1) != 2 || proverka != '\n')
                {
                    qDebug() << "Error!" << endl;
                    while (getchar() != '\n');
                }
                bool a = function2(n);
                if (a)
                    qDebug() << n << endl;
                break;
            }
            case 3:
            {
                qDebug() << "Enter the number" << endl;
                while (scanf("%d%c", &n, &proverka, 1) != 2 || proverka != '\n')
                {
                    qDebug() << "Error!" << endl;
                    while (getchar() != '\n');
                }
                bool a = function3(n);
                if (a)
                    qDebug() << n << endl;
                break;
            }
            case 4:
            {
                qDebug() << "Enter the number" << endl;
                while (scanf("%d%c", &n, &proverka, 1) != 2 || proverka != '\n')
                {
                    qDebug() << "Error!" << endl;
                    while (getchar() != '\n');
                }
                bool a = function4(n);
                if (a)
                    qDebug() << n << endl;
                break;
                }
            case 5:
            {
                qDebug() << "Enter the number" << endl;
                while (scanf("%d%c", &n, &proverka, 1) != 2 || proverka != '\n')
                {
                    qDebug() << "Error!" << endl;
                    while (getchar() != '\n');
                }
                bool a = function5(n);
                if (a)
                    qDebug() << hex << n << endl;
                break;
            }
            case 6:
            {
                    qDebug() << "Enter the number, use English letters" << endl;
                    string letters = "0123456789ABCDEF";
                    string v;
                    cin >> v;
                    bool eee = false;
                    for (int i = 0; i < v.length(); i++)
                    {
                        eee = false;
                        for (int j = 0; j < letters.length(); j++)
                        {
                            if (v[i] == letters[j])
                                eee = true;
                        }
                        if (!eee)
                        {
                            qDebug() << "Error! number incorrectly written" << endl;
                            break;
                        }
                        eee = true;
                    }
                    if (!eee)
                        break;
                    n = 0;
                    int unit = 0;
                    for (int i = 0; i < v.length(); i++)
                    {
                        for (int j = 0; j < letters.length(); j++)
                        {
                            if (v[i] == letters[j])
                            {
                                unit = j;
                            }
                        }
                        n = n + unit * pow(16,(v.length() - i - 1));

                    }
                    bool a = function6(n);
                    if (a)
                        qDebug() << n << endl;
                    break;
            }
            case 0:
                break;
        }
    }while (menu != 0);

    qDebug() << "It works well";
    return 0;
}
