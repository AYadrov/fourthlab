#include "functions.h"
#include <cmath>
#include <iostream>
#include <cstring>
#include <QDebug>
using namespace std;

//convert 2 to 10
bool function1(int &n)
{
    int r1 = 2; int  r2 = 10;
    int k, m, j, res;
    k = 0; res = 0;

    m = n;
    j = n;
    int size = 0;
    while (m > 0)
    {
        size++;
        m = m / 10;
    }
    for (int i = 0; i < size; i++)
    {
        m = j % 10;
        j = j / 10;
        if ((m != 1) && (m != 0))
        {
            qDebug() << "Error! number incorrectly written";
            return false;
        }
    }


    while (n > 0)
        {
            res += n % 10 * pow(r1, k);
            k++;
            n /= 10;
        }

        k = 1; n = 0;
        while (res > 0)
        {
            n += res % r2 * k;
            k *= 10;
            res /= r2;
        }
    return true;
}
//convert 10 to 2
bool function2(int &n)
{
    int r1 = 10; int  r2 = 2;
        int k, m, j, res;
        k = 0; res = 0;

        m = n;
        j = n;
        int size = 0;
        while (m > 0)
        {
            size++;
            m = m / 10;
        }
        for (int i = 0; i < size; i++)
        {
            m = j % 10;
            j = j / 10;
            if ((m > 9) || (m < 0))
            {
                qDebug() << "Error! number incorrectly written";
                return false;
            }
        }


        while (n > 0)
            {
                res += n % 10 * pow(r1, k);
                k++;
                n /= 10;
            }

            k = 1; n = 0;
            while (res > 0)
            {
                n += res % r2 * k;
                k *= 10;
                res /= r2;
            }
        return true;
}
//convert 2 to 8
bool function3(int &n)
{
    int r1 = 2; int  r2 = 8;
    int k, m, j, res;
    k = 0; res = 0;

    m = n;
    j = n;
    int size = 0;
    while (m > 0)
    {
        size++;
        m = m / 10;
    }
    for (int i = 0; i < size; i++)
    {
        m = j % 10;
        j = j / 10;
        if ((m != 1) && (m != 0))
        {
            qDebug() << "Error! number incorrectly written";
            return false;
        }
    }


    while (n > 0)
        {
            res += n % 10 * pow(r1, k);
            k++;
            n /= 10;
        }

        k = 1; n = 0;
        while (res > 0)
        {
            n += res % r2 * k;
            k *= 10;
            res /= r2;
        }
    return true;
}
//convert 8 to 2
bool function4(int &n)
{
    int r1 = 8; int  r2 = 2;
    int k, m, j, res;
    k = 0; res = 0;

    m = n;
    j = n;
    int size = 0;
    while (m > 0)
    {
        size++;
        m = m / 10;
    }
    for (int i = 0; i < size; i++)
    {
        m = j % 10;
        j = j / 10;
        if ((m > 7) || (m < 0))
        {
            qDebug() << "Error! number incorrectly written";
            return false;
        }
    }


    while (n > 0)
        {
            res += n % 10 * pow(r1, k);
            k++;
            n /= 10;
        }

        k = 1; n = 0;
        while (res > 0)
        {
            n += res % r2 * k;
            k *= 10;
            res /= r2;
        }
    return true;
}
//convert 8 to 16
bool function5(int& n)
{

    int r1 = 8; int r2 = 10;
    int k, res, j, m;
    k = 0; res = 0;

    m = n;
    j = n;
    int size = 0;
    while (m > 0)
    {
        size++;
        m = m / 10;
    }
    for (int i = 0; i < size; i++)
    {
        m = j % 10;
        j = j / 10;
        if ((m > 7) || (m < 0))
        {
            qDebug() << "Error! number incorrectly written";
            return false;
        }
    }

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
    return true;
}

//convert 16 to 8
bool function6(int& n)
{

    int r1 = 10; int r2 = 8;
    int k, res, j, m;
    k = 0; res = 0;

    m = n;
    j = n;
    int size = 0;
    while (m > 0)
    {
        size++;
        m = m / 10;
    }
    for (int i = 0; i < size; i++)
    {
        m = j % 10;
        j = j / 10;
        if ((m > 9) || (m < 0))
        {
            qDebug() << "Error! number incorrectly written";
            return false;
        }
    }

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
    return true;
}
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
// 10
// 11
// 12
// 13
